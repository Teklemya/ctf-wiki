// gcc -nostartfiles -fno-stack-protector -no-pie
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

void _start()
{
    int i = 0;
    char buf[32];
    read(0, buf, 100);

    if (i == 0xf00dbabe)
    {
        puts("win\n");
    }
    else
    {
        printf("failed: %x\n", i);
    }

    exit(0);
}
